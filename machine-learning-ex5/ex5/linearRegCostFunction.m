function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%












% =========================================================================

%from ex1 computeCost.m
prediction = X*theta;
cost = prediction - y;

squareoferror = cost.^2;

sumoferrors = sum(squareoferror);

J = sumoferrors/(2*m);

%from ex2 costFunctionReg.m
sumthetasquared = 0;

j = length(theta);
for k = 2:j
  sumthetasquared = sumthetasquared + theta(k)^2;
end

J = J + sumthetasquared*(lambda/(2*m));


%from ex1/gradientDescentMulti.m
[xrow, xcol] = size(X);
    
for i = 1:xcol
  costX = cost .* X(:,i);
  sumcostX = sum(costX(:,1));
      
  if(i == 1)
    grad = [sumcostX/m];
  else
    %regularization term from ex2 costFunctionReg.m
    grad = [grad; sumcostX/m + (lambda/m)*theta(i)];
  end
      
end

grad = grad(:);

end
