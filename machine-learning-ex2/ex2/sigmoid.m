function g = sigmoid(z)
%SIGMOID Compute sigmoid function
%   g = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
g = zeros(size(z));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the sigmoid of each value of z (z can be a matrix,
%               vector or scalar).





% =============================================================
[xrow,ycol] = size(z);

for i = 1:xrow
  for j = 1:ycol
    g(i,j) = 1/(1 + e.^(-z(i,j)));
  end
end

% this is a vectorized/better implementation copied from ex3 sigmoid.m
%g = 1 ./ (1 + e.^(-z) );

%g

end
