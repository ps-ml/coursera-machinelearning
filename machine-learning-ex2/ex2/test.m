%test cases available here: https://www.coursera.org/learn/machine-learning/discussions/all/threads/JHorKeknEeS0tyIAC9RBcw/replies/ZKJxyBaPEeWYoCIAC5MBcw

% Initialization
clear ; close all; clc
%theta = [1;2;3];
%x = [0;0;0];
%thetatransposex = theta'*x
%sigmoid([1;2;3]);
%sigmoid(-5)
%sigmoid(5)
%sigmoid([4 5 6])
%V = reshape(-1:.1:.9, 4, 5);
%sigmoid(V)
X = [1 1 ; 1 2.5 ; 1 3 ; 1 4];
theta = [-3.5 ; 1.3];
predict(theta, X)