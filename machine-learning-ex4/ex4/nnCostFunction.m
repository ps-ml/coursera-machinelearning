function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%



















% -------------------------------------------------------------

% =========================================================================

X = [ones(m, 1) X];
z2 = X*Theta1';
a2 = sigmoid(z2);

a2 = [ones(size(a2, 1), 1) a2];

a3 = sigmoid(a2*Theta2');

%Ref: ex4.pdf : Also, recall that whereas the original labels (in the variable y) were 1, 2, ..., 10, for the purpose of training a neural network, we need to recode the labels as vectors containing only values 0 or
%1, so that
%Ref: https://www.coursera.org/learn/machine-learning/programming/AiHgN/neural-network-learning/discussions/threads/QFnrpQckEeWv5yIAC00Eog
y_matrix = eye(num_labels)(y,:); 

J = sum((sum(-y_matrix.* (log(a3)) -(1-y_matrix).*(log(1-a3)))))/m; 

%The following non-vectorized function also works:
%for i = 1: m
  %for k = 1:num_labels
    %fprintf('%f ', k);
    %fprintf('%f ', i);
    
   % J = J + (-y_matrix(i,k)*(log(a3(i,k))) - ((1  - y_matrix(i,k))*(log(1-a3(i,k)))) );  
  %end
%end

regularizationterm = 0;

Theta1withoutBias = Theta1(:,2:end);
Theta2withoutBias = Theta2(:,2:end);

regularizationterm = sum( sum(Theta1withoutBias.^2) ) +  sum (sum(Theta2withoutBias.^2) );
regularizationterm = regularizationterm*lambda/(2*m);

%The following non-vectorized implementation also works.
%[r,c] = size(Theta1withoutBias);
%sum = 0;
%for j = 1: r
  %for k = 1:c
    %sum = sum + Theta1withoutBias(j,k)^2;  
  %end
%end

%[r,c] = size(Theta2withoutBias);
%for j = 1: r
  %for k = 1:c
    %sum = sum + Theta2withoutBias(j,k)^2;  
  %end
%end

%regularizationterm = lambda*regularizationterm/(2*m);


J = J + regularizationterm;

% as per ex4.pdf we are supposed to implement a for loop here. However, the tutorials ( comments section ) says that the for loop is too difficult to implement and we should use the vectorized approach
% so, implementing the vectorized approach here

%δ3 or d3 is the difference between a3 and the y_matrix. The dimensions are the same as both, (m x r).
d3 = a3 - y_matrix;

%δ2 or d2 is tricky. It uses the (:,2:end) columns of Theta2. d2 is the product of d3 and Theta2(no bias), then element-wise scaled by sigmoid gradient of z2. The size is (m x r) ⋅ (r x h) --> (m x h). The size is the same as z2, as must be.
d2 = d3*Theta2withoutBias;

%z2 came from the forward propagation process - it's the product of a1 and Theta1, prior to applying the sigmoid() function. Dimensions are (m x n) ⋅ (n x h) --> (m x h)
d2 = d2.*sigmoidGradient(z2);

%Δ1 or Delta1 is the product of d2 and a1. The size is (h x m) ⋅ (m x n) --> (h x n)
delta1 = d2'*X;

%Δ2 or Delta2 is the product of d3 and a2. The size is (r x m) ⋅ (m x [h+1]) --> (r x [h+1])
delta2 = d3'*a2;

%Theta1_grad and Theta2_grad are the same size as their respective Deltas, just scaled by 1/m.
Theta1_grad = delta1/m;
Theta2_grad = delta2/m;


% as per ex4.pdf and the tutorial for j = 0, the regularization term is 0
Theta1BackPropRegularization = Theta1;
Theta1BackPropRegularization(:,1) = 0;

Theta2BackPropRegularization = Theta2;
Theta2BackPropRegularization(:,1) = 0;

%see ex4.pdf section 2.5
Theta1_grad = Theta1_grad + (lambda/m)*(Theta1BackPropRegularization);
Theta2_grad = Theta2_grad + (lambda/m)*(Theta2BackPropRegularization);

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
