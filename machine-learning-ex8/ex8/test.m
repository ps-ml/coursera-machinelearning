%all test cases:https://www.coursera.org/learn/machine-learning/discussions/all/threads/0SxufTSrEeWPACIACw4G5w
%all tutorials: https://www.coursera.org/learn/machine-learning/discussions/all/threads/m0ZdvjSrEeWddiIAC9pDDA

clear ; close all; clc
#X = sin(magic(4));
#X = X(:,1:3);
#[mu sigma2] = estimateGaussian(X)

#[epsilon F1] = selectThreshold([1 0 0 1 1]', [0.1 0.2 0.3 0.4 0.5]')

%params = [ 1:14 ] / 10;
%Y = magic(4);
%Y = Y(:,1:3);
%R = [1 0 1; 1 1 1; 0 0 1; 1 1 0] > 0.5;     % R is logical
%num_users = 3;
%num_movies = 4;
%num_features = 2;
%lambda = 6;
%J = cofiCostFunc(params, Y, R, num_users, num_movies, num_features, lambda)

params = [ 1:14 ] / 10;
Y = magic(4);
Y = Y(:,1:3);
R = [1 0 1; 1 1 1; 0 0 0; 1 1 0] > 0.5;     % R is logical
num_users = 3;
num_movies = 4;
num_features = 2;
lambda = 6;
[J, grad] = cofiCostFunc(params, Y, R, num_users, num_movies, num_features, lambda)