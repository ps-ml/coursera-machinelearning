function [theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters)
%GRADIENTDESCENTMULTI Performs gradient descent to learn theta
%   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCostMulti) and gradient here.
    %

    prediction = X*theta;
    cost = prediction - y;
    
    [xrow, xcol] = size(X);
    
    for i = 1:xcol
      costX = cost .* X(:,i);
      sumcostX = sum(costX(:,1));
      
      if(i == 1)
        jthetaPartialDifferential = [sumcostX/m];
      else
        jthetaPartialDifferential = [jthetaPartialDifferential; sumcostX/m];
      end
      
    end
    
    theta = theta - alpha*jthetaPartialDifferential;
    
    % Save the cost J in every iteration    
    J_history(iter) = computeCostMulti(X, y, theta);
    

end

end
