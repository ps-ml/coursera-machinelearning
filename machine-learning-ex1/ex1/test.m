%example from https://www.coursera.org/learn/machine-learning/discussions/all/threads/5wftpZnyEeWKNwpBrKr_Fw
% [Xn mu sigma] = featureNormalize([1 ; 2 ; 3])

%[Xn mu sigma] = featureNormalize(magic(3));

%[Xn mu sigma] = featureNormalize([-ones(1,3); magic(3)])


% computeCostMulti example from https://www.coursera.org/learn/machine-learning/discussions/all/threads/5wftpZnyEeWKNwpBrKr_Fw
%X = [ 2 1 3; 7 1 9; 1 8 1; 3 7 4 ];
%y = [2 ; 5 ; 5 ; 6];
%theta_test = [0.4 ; 0.6 ; 0.8];
%computeCostMulti( X, y, theta_test )

%gradientDescentMulti() w/ zeros for initial_theta
#X = [ 2 1 3; 7 1 9; 1 8 1; 3 7 4 ];
#y = [2 ; 5 ; 5 ; 6];
#[theta, J_hist] = gradientDescentMulti(X, y, zeros(3,1), 0.01, 10);

% gradientDescentMulti() with non-zero initial_theta
#X = [ 2 1 3; 7 1 9; 1 8 1; 3 7 4 ];
#y = [2 ; 5 ; 5 ; 6];
#[theta J_hist] = gradientDescentMulti(X, y, [0.1 ; -0.2 ; 0.3], 0.01, 10);

X = [ 2 1 3; 7 1 9; 1 8 1; 3 7 4 ];
y = [2 ; 5 ; 5 ; 6];
theta = normalEqn(X,y)



