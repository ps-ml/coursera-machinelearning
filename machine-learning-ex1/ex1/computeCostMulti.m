function J = computeCostMulti(X, y, theta)
%COMPUTECOSTMULTI Compute cost for linear regression with multiple variables
%   J = COMPUTECOSTMULTI(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
prediction = X*theta;
cost = prediction - y;

squareoferror = cost.^2;

sumoferrors = sum(squareoferror);

% You need to return the following variables correctly 
J = sumoferrors/(2*m)

%fprintf('Program paused. Press enter to continue.\n');

 %     pause;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.





% =========================================================================

end
