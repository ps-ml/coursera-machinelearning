function [theta, J_history] =  gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %







    % ============================================================
    derivativeTheta0 = 0;
    derivativeTheta1 = 0;
    for i = 1: m
      prediction = theta(1,1) + theta(2,1)*X(i,2);
      cost = prediction - y(i);
      derivativeTheta0 = derivativeTheta0 + cost;
      derivativeTheta1 = derivativeTheta1 + cost*X(i,2);
    end
    
    derivativeTheta0 = derivativeTheta0/m;
    derivativeTheta1 = derivativeTheta1/m;
    
    theta0 = theta(1,1) - alpha*derivativeTheta0;
    theta1 = theta(2,1) - alpha*derivativeTheta1;
    
    theta = [theta0; theta1];
    
    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);
    

end

end
