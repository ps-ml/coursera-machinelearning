% all test case available here: https://www.coursera.org/learn/machine-learning/discussions/all/threads/0SxufTSrEeWPACIACw4G5w

% Initialization
clear ; close all; clc
%sigmoid([1;2;3])
%sigmoid(-5)
%sigmoid(0)
%sigmoid(5)
%sigmoid([4 5 6])
%V = reshape(-1:.1:.9, 4, 5);
%sigmoid(V)


%Ref: https://www.coursera.org/learn/machine-learning/discussions/all/threads/GGb4CQPFEeWv5yIAC00Eog
%theta = [-2; -1; 1; 2];
%X = [ones(3,1) magic(3)];
%y = [1; 0; 1] >= 0.5;       % creates a logical array
%lambda = 3;
%[J grad] = lrCostFunction(theta, X, y, lambda);

%J

%grad

%X = magic(3)
%X = X(:)

%input:
X = [magic(3) ; sin(1:3); cos(1:3)];
y = [1; 2; 2; 1; 3];
num_labels = 3;
lambda = 0.1;
[all_theta] = oneVsAll(X, y, num_labels, lambda)