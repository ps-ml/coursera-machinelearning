function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Hint: The computation of the cost function and gradients can be
%       efficiently vectorized. For example, consider the computation
%
%           sigmoid(X * theta)
%
%       Each row of the resulting matrix will contain the value of the
%       prediction for that example. You can make use of this to vectorize
%       the cost function and gradient computations. 
%
% Hint: When computing the gradient of the regularized cost function, 
%       there're many possible vectorized solutions, but one solution
%       looks like:
%           grad = (unregularized gradient for logistic regression)
%           temp = theta; 
%           temp(1) = 0;   % because we don't add anything for j = 0  
%           grad = grad + YOUR_CODE_HERE (using the temp variable)
%

% =============================================================

%this converts an xy-matrix into a y-vector
%grad = grad(:);

%for i = 1:m
  %x = X(i,: );
  %h = sigmoid(x*theta);
  %J = J + -y(i).*(log(h)) - (1-y(i)).*(log(1 - h));
%end

% The following is a vectorized implementation of the code above. The code above is from ex2.   
H = sigmoid(X*theta);

J = sum(-y.* (log(H)) -(1-y).*(log(1-H)));

J = J/m;


thetaforlambda = theta(2:end,:);

%sumthetasquared = 0;
%for k = 2:j
  %sumthetasquared = sumthetasquared + theta(j)^2;
%end

% The following is a vectorized implementation of the code above. The code above is from ex2.   
sumthetasquared = sum(thetaforlambda.^2);

J = J + sumthetasquared*(lambda/(2*m));

prediction = sigmoid(X*theta);
   %this is matrix of cost and not the same as J
   cost = prediction - y;
    
   %[xrow, xcol] = size(X);
    
   %for j = 1:xcol
     %costX = cost .* X(:,j);
     %sumcostX = sum(costX);
      
     %if(j == 1)
       %grad = [sumcostX/m];
     %else
       %a = sumcostX/m
       %b = (lambda/m)*theta(j)
       %grad = [grad; sumcostX/m + (lambda/m)*theta(j)];
     %end
      
   %end
   
% The following is a vectorized implementation of the code above. The code above is from ex2.   
costX = cost.* X;
sumCostX = sum(costX)/m;%this is columnwise addition: Ref:https://www.mathworks.com/help/matlab/ref/sum.html
grad1 = sumCostX(:,1);
thetaforgrad = theta(2:end,:)';
thetaforgrad = (lambda/m).* thetaforgrad;
sumCostX = sumCostX(:,2:end);

grad2 = sumCostX + thetaforgrad;
grad = [grad1,grad2];
grad = grad(:);

end
